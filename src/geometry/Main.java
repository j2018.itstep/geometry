package geometry;

import geometry.figures.Geometry;
import geometry.figures.Line;
import geometry.figures.Point;
import geometry.figures.Polygon;
import geometry.figures.Triangle;

public class Main {
	public static void main(String[] args) {
		Point point = new Point("Point", 0, 0);
		Line line = new Line("Line", point, point);
		Polygon polygon = new Polygon("Triangle", new Point("Triangle", -3, 0), new Point("Triangle", 3, 0), new Point("Triangle", 0, 4));
		System.out.println(polygon.calculatePerimeter());
//		System.out.println(line.getCenterX());
//		System.out.println(point.getCenterY());
	}
}
