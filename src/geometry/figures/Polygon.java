package geometry.figures;

public class Polygon extends Shape {
	protected Point[] points;

	public Polygon(String name, Point... points) {
		super(name, 0, 0);
		this.points = points;

	}

	public Point[] getPoints() {
		return points;
	}

	@Override
	public double calculateArea() {
		return 0;
	}

	@Override
	public double calculatePerimeter() {
		double sum = 0;
		for (int i = 0; i < points.length - 1; i++) {
			sum += points[i].calculateDistance(points[i + 1]);
		}
		sum += points[0].calculateDistance(points[points.length - 1]);
		return sum;
	}

}
