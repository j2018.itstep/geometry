package geometry.figures;

public class Point extends Geometry {

	public Point(String name, double centerX, double centerY) {
		super(name, centerX, centerY);

	}

	public double calculateDistance(Point point) {
		double xDiff = this.getCenterX() - point.getCenterX();
		double yDiff = this.getCenterY() - point.getCenterY();
		return Math.sqrt(Math.pow(xDiff, 2) + Math.pow(yDiff, 2));
	}
}
